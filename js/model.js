function SinhVien(ma, ten, mail, matKhau, toan, ly, hoa) {
  this.ma = ma;
  this.ten = ten;
  this.email = mail;
  this.matKhau = matKhau;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.tinhDiemTB = function () {
    return Math.round(((this.toan + this.ly + this.hoa) / 3) * 100) / 100;
  };
}
