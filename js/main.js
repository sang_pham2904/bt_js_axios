const BASE_URL = "https://63f442defe3b595e2ef03bc0.mockapi.io";
function fetchDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      // console.log("res.data: ", res.data);
      let dssv = res.data.map(function (item) {
        return new SinhVien(
          item.ma,
          item.ten,
          item.email,
          item.matKhau,
          item.toan,
          item.ly,
          item.hoa
        );
      });
      tatLoading();
      render(dssv);
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatLoading();
    });
}
fetchDSSV();

// Thêm SV
function themSV() {
  let sv = layTTTuForm();
  batLoading(),
    axios({
      url: `${BASE_URL}/sv`,
      method: "POST",
      data: sv,
    })
      .then(function (res) {
        tatLoading();
        fetchDSSV();
      })
      .catch(function (err) {
        tatLoading();
        console.log("err: ", err);
      });
  reset();
}
// Xóa SV
function xoaSV(id) {
  batLoading(),
    axios({
      url: `${BASE_URL}/sv/${id}`,
      method: "DELETE",
    })
      .then(function (res) {
        tatLoading();
        fetchDSSV();
      })
      .catch(function (err) {
        tatLoading();
        console.log("err: ", err);
      });
}
// sửa SV
function suaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      // console.log("res: ", res.data);
      tatLoading();
      let sv = res.data;
      document.getElementById("txtMaSV").value = sv.ma;
      document.getElementById("txtTenSV").value = sv.ten;
      document.getElementById("txtEmail").value = sv.email;
      document.getElementById("txtPass").value = sv.matKhau;
      document.getElementById("txtDiemToan").value = sv.toan;
      document.getElementById("txtDiemLy").value = sv.ly;
      document.getElementById("txtDiemHoa").value = sv.hoa;
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}

function capNhat() {
  let sv = layTTTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      tatLoading();
      console.log("res: ", res.data);
      fetchDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
  reset();
}

// Search SV
document.getElementById("btnSearch").addEventListener("click", function () {
  let inputSearch = document.getElementById("txtSearch").value.toLowerCase();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      let dssv = res.data.map(function (item) {
        return new SinhVien(
          item.ma,
          item.ten,
          item.email,
          item.matKhau,
          item.toan,
          item.ly,
          item.hoa
        );
      });
      let filterDSSV = dssv.filter(function (item) {
        return item.ten.toLowerCase().includes(inputSearch);
      });
      if (filterDSSV.length > 0) {
        render(filterDSSV);
      }
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
});
