function layTTTuForm() {
  // lấy giá trị user nhập vào
  let ma = document.getElementById("txtMaSV").value;
  let ten = document.getElementById("txtTenSV").value;
  let email = document.getElementById("txtEmail").value;
  let matKhau = document.getElementById("txtPass").value;
  let toan = Number(document.getElementById("txtDiemToan").value);
  let ly = Number(document.getElementById("txtDiemLy").value);
  let hoa = Number(document.getElementById("txtDiemHoa").value);
  //   tạo obj chứa các giá trị đó
  return new SinhVien(ma, ten, email, matKhau, toan, ly, hoa);
}
function render(danhSachSV) {
  let sinhVien = "";
  for (let i = danhSachSV.length - 1; i >= 0; i--) {
    sinhVien += `<tr>
        <td>
        ${danhSachSV[i].ma}
        </td>
        <td>
        ${danhSachSV[i].ten}
        </td>
        <td>
        ${danhSachSV[i].email}
        </td>
        <td>
        ${danhSachSV[i].tinhDiemTB()}
        </td>
        <td>
        <button class = "btn btn-danger" onclick ="xoaSV('${
          danhSachSV[i].ma
        }')">
        Xóa
        </button>
        <button class = "btn btn-primary" onclick ="suaSV('${
          danhSachSV[i].ma
        }')">
        Sửa
        </button>
        </td>
        </tr>`;
  }
  document.getElementById("tbodySinhVien").innerHTML = sinhVien;
}
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function reset() {
  document.getElementById("formQLSV").reset();
}
